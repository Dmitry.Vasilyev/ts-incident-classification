import re
import wordcloud as wc
from gensim.parsing.preprocessing import preprocess_string

import pandas as pd
from gensim import corpora, models, parsing
import numpy as np
import copy
from gensim.parsing.preprocessing import preprocess_string
from gensim.parsing.preprocessing import preprocess_string
from gensim.parsing.preprocessing import strip_tags,strip_punctuation,strip_multiple_whitespaces, strip_numeric,remove_stopwords,strip_short, stem_text

#from gensim.utils import simple_preprocess
import nltk
from nltk.stem import WordNetLemmatizer 
#nltk.download('wordnet')
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import pymorphy2
from pymorphy2 import MorphAnalyzer
import operator
from operator import itemgetter


### int|float to string ####
def intfloat_to_str(texts):

    i = 0
    num_ = 0
    for doc in texts:
        num_ +=1
        if type(doc) == int or type(doc) == float :
            texts[num_-1] = str(doc)
    return texts
        
#Нормализация
def to_normal(texts):
    morph = pymorphy2.MorphAnalyzer()
    lem_text = []
    for texts_i in texts:
        
        texts_i = preprocess_string(texts_i, [lambda x: x.lower(), strip_punctuation, strip_short ])
        lem_text_i = []
    
        for i in texts_i:            
            lem_text_i.append(morph.parse(i)[0].normal_form)
        lem_text.append(lem_text_i)
    return lem_text


# cтоп-слова 
#nltk.download('stopwords')
# from nltk.corpus import stopwords 
# from nltk.tokenize import word_tokenize 
def del_stop_words(lem_text):
    stop_words = set(stopwords.words('russian')) 
    filtered_sentence_corp = []
    filtered_sentence = []
    for text_token in lem_text:
        filtered_sentence = []
        for tokens_ in text_token:
            if tokens_ not in stop_words: 
                filtered_sentence.append(tokens_)
        filtered_sentence_corp.append(filtered_sentence)
    return filtered_sentence_corp


# цифры
def del_digits(text):
    ret_text = []
    for i in text:
        not_del = []
        for j in i:
            if not j.isdigit():
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text 


# цифрослова
def del_digwords(text):
    ret_text = []
    filt = '.\d+'
    for i in text:
        not_del = []
        for j in i:
            if not re.search(filt,j):
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text

# какие-то слова по желанию
def del_some_words(text):
    ret_text = []
    words =['добрый','день','нижегородский','ребята','екатеринбургский','это','additional', \
            'information','not','edit','empty','просьба','просить','документ']
    for i in text:
        not_del = []
        for j in i:
            if j not in words:
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text


# имена
names = pd.read_csv('./input/russian_names.csv', sep =';' ) 
other_names = pd.read_csv('./input/foreign_names.csv', sep =';' ) 

def del_names(text):
    ret_text = []
    to_small = lambda x: x.lower()
    nam = names['Name'].transform(to_small)
    words = nam.to_list()
    for i in text:
        not_del = []
        for j in i:
            if j not in words:
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text


# фамилии
surnames = pd.read_csv('./input/russian_surnames.csv', sep =';' ) 

def del_surnames(text):
    ret_text = []
    to_small = lambda x: x.lower()
    sur = surnames['Surname'].transform(to_small)
    words = sur.to_list()
    for i in text:
        not_del = []
        for j in i:
            if j not in words:
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text


# english
to_save_eng = ['fobo', 'sap', 'web', 'pos', 'erp', 'tspos', 'idoc','rlo','irm']
def del_eng(text):
    ret_text = []
    filt = '[a-zA-Z]+'
    for i in text:
        not_del = []
        for j in i:
            if not re.search(filt,j) or j in to_save_eng:
                not_del.append(j)
        ret_text.append(not_del)
    return ret_text


# pipeline всех преобразований
def pipeline_text_to_norm(text):
    ret_text = intfloat_to_str(text)
    ret_text = to_normal(ret_text)
    ret_text = del_stop_words(ret_text)
    ret_text = del_digits(ret_text)
    ret_text = del_digwords(ret_text)
    ret_text = del_eng(ret_text)
    ret_text = del_some_words(ret_text)
    ret_text = del_names(ret_text)
    ret_text = del_surnames(ret_text)
    return ret_text