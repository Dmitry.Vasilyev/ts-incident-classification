def as_sparse_vector(dictionary,vector):
    # функция для одного обращения
    res_v = []
    for g in range(len(dictionary)):
        yes = 0 
        for k in vector: 
            #print(g,k)
            if g == k[0]:
                yes = 1
                to_add = k[1]
        if yes == 1:
            res_v.append(to_add)
        else: 
            res_v.append(0)

    return res_v

def as_sparse_matrix(dictionary,tf_idf_vectors):
    sparse_m = []
    for v in tf_idf_vectors:
        sparse_v = as_sparse_vector(dictionary,v)
        sparse_m.append(sparse_v)
    return sparse_m  